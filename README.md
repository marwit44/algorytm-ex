# algorytm-ex

Application AlgorytmEx is written with usage of Spring Boot technology.

It is simple example of four algorithms: factorial calculation, checking palindrom words property,
bubble sort and check graph consistency.

Input of data is possible from file or input field, but it depends of algorithm example. 
Application shows informal code of each example.

Example input files are in:
/algorytm-ex/src/main/resources/static/files/examples
