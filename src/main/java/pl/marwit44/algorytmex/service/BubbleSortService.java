package pl.marwit44.algorytmex.service;

import org.springframework.stereotype.Service;

@Service
public class BubbleSortService {

    public String getSortedResult(String value) {

        value = value.replaceAll("\n"," ");

        String[] valueStr = value.split(" ");

        int n = valueStr.length;
        int[] tab = new int[n];

        for (int i = 0; i < n; i++) {
            tab[i] = Integer.parseInt(valueStr[i]);
        }

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < n - i; j++) {
                if (tab[j - 1] > tab[j]) {
                    int tmp = tab[j - 1];
                    tab[j - 1] = tab[j];
                    tab[j] = tmp;
                }
            }
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n; i++) {
            result.append(tab[i]).append(" ");
        }

        return result.toString();
    }
}
