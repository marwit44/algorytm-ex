package pl.marwit44.algorytmex.service;

import org.springframework.stereotype.Service;

@Service
public class PalindromeService {
    public String checkText(String value) {

        int n = value.length();
        int m = value.length() / 2;

        for (int i = 0; i < m; i++) {
            if(value.charAt(i) != value.charAt(n-i-1)){
                return "No";
            }
        }

        if(value.equals("")) {
            return "";
        }

        return "Yes";
    }

    public String prepareText(String value) { return value.replaceAll("[ .,-?!\"\n]","").toLowerCase(); }
}
