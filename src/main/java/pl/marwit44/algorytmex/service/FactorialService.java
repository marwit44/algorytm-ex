package pl.marwit44.algorytmex.service;

import org.springframework.stereotype.Service;

@Service
public class FactorialService {

    public double getResultIterative(int value) {

        double result = 1;

        for (int i = 1; i <= value; i++) {
            result *= i;
        }

        return result;
    }

    public double getResultRecursive(int value) {

        if (value == 0) {
            return 1;
        }

        if (value > 0) {
            return value * getResultRecursive(value - 1);
        }

        return -1;
    }
}
