package pl.marwit44.algorytmex.service;

import org.springframework.stereotype.Service;

import java.util.Stack;

@Service
public class GraphConsistencyService {

    public String checkConsistency(String[] row) {

        int n = row.length;
        int m = 0;

        if (row.length > 0) {
            m = row[0].split(" ").length;
        }

        int[][] tab = new int[n][m];

        for (int i = 0; i < n; i++) {
            String[] column = row[i].split(" ");
            for (int j = 0; j < m; j++) {
                tab[i][j] = Integer.parseInt(column[j]);
                if (j < n) {
                    tab[j][i] = tab[i][j];
                }
                if (tab[i][j] < 0 || tab[i][j] > 1) {
                    return "";
                }
            }
        }

        int[] visited = new int[n];

        for (int i = 0; i < n; i++) {
            visited[i] = 0;
        }

        Stack<Integer> stack = new Stack<>();
        int vc = 0;

        stack.push(0);
        visited[0] = 1;

        while (!stack.empty()) {
            int v = stack.pop();
            vc++;

            for (int i = 0; i < n; i++) {
                if (tab[v][i] == 1) {
                    if (visited[i] == 1) {
                        continue;
                    }
                    visited[i] = 1;
                    stack.push(i);
                }
            }
        }

        if (vc == n) {
            return "Yes";
        } else {
            return "No";
        }
    }

    public String[] addColumnNumbers(String value) {
        int n = value.split("\n").length;
        StringBuilder columnsAndValue = new StringBuilder();

        for (int i = 0; i < n; i++) {
            columnsAndValue.append(i + 1).append(" ");
        }

        columnsAndValue.append("\n");
        columnsAndValue.append(value);

        return columnsAndValue.toString().split("\n");
    }

    public String[] prepareText(String fileContent) {
        return fileContent.split("\n");
    }
}
