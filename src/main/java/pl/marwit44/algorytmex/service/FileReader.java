package pl.marwit44.algorytmex.service;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Service
public class FileReader {

    public String readFromFile(String fileName) throws IOException {

        InputStream inputStream = new ClassPathResource("/static/files/" + fileName + ".html").getInputStream();

        return getContentFile(inputStream);
    }

    public String readFromMultipartFile(MultipartFile multipartFile) throws IOException {

        InputStream inputStream = multipartFile.getInputStream();

        return getContentFile(inputStream);
    }

    private String getContentFile(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }

        inputStream.close();

        return resultStringBuilder.toString();
    }

    public String[] prepareAlgorithmText(String data, int part, int partsNumber) {

        String[] dataStr = data.split("\n");

        int n = dataStr.length / partsNumber;
        String[] dataPart = new String[n];
        int j = 0;

        for (int i = (part - 1) * n; i < part * n; i++) {
            dataPart[j] = dataStr[i];
            j++;
        }

        return dataPart;
    }
}
