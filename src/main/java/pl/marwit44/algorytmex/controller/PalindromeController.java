package pl.marwit44.algorytmex.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import pl.marwit44.algorytmex.AlgorithmData;
import pl.marwit44.algorytmex.service.FileReader;
import pl.marwit44.algorytmex.service.PalindromeService;

import java.io.IOException;
import java.util.Objects;

@Controller
@RequestMapping("/palindrome")
public class PalindromeController {

    private final Logger log = LoggerFactory.getLogger(PalindromeController.class);

    private final PalindromeService palindromeService;
    private final FileReader fileReader;

    @Autowired
    public PalindromeController(PalindromeService palindromeService, FileReader fileReader) {
        this.palindromeService = palindromeService;
        this.fileReader = fileReader;
    }

    @GetMapping
    public String getPalindrome(Model model) {

        AlgorithmData algorithmData = new AlgorithmData("", 1);

        model.addAttribute("result", "");
        model.addAttribute("algorithmData", algorithmData);

        return "palindrome";
    }

    @PostMapping
    public String getResult(@ModelAttribute MultipartFile file, @ModelAttribute AlgorithmData algorithmData, Model model) {

        String result = "";
        String fileContent = "";
        String value = "";
        String[] algorithm = new String[0];

        try {
            if (algorithmData.getType() == 2) {
                if (file != null) {
                    if (Objects.equals(file.getContentType(), "text/plain")) {
                        fileContent = fileReader.readFromMultipartFile(file);
                    } else {
                        model.addAttribute("exception", "uploaded file is not text file");
                    }

                    if (Objects.equals(file.getOriginalFilename(), "")) {
                        model.addAttribute("exception", "no file uploaded");
                    }

                    if (!fileContent.equals("")) {
                        value = palindromeService.prepareText(fileContent);
                        result = palindromeService.checkText(value);
                    }

                    model.addAttribute("fileName", file.getOriginalFilename());
                }
            } else if (algorithmData.getType() == 1) {
                if (algorithmData.getValue() != null && !algorithmData.getValue().equals("")) {
                    value = palindromeService.prepareText(algorithmData.getValue());
                    result = palindromeService.checkText(value);
                }
            }
            String data = fileReader.readFromFile("palindromeAlgorithm");
            algorithm = fileReader.prepareAlgorithmText(data, 1, 1);
        } catch (IOException e) {
            log.error(e.getMessage());
            model.addAttribute("exception", e.getMessage());
        }

        model.addAttribute("algorithm", algorithm);
        model.addAttribute("algorithmData", algorithmData);
        model.addAttribute("fileContent", fileContent);
        model.addAttribute("value", value);
        model.addAttribute("result", result);

        return "palindrome";
    }
}
