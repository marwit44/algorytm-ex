package pl.marwit44.algorytmex.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.marwit44.algorytmex.AlgorithmData;
import pl.marwit44.algorytmex.service.FactorialService;
import pl.marwit44.algorytmex.service.FileReader;

import java.io.IOException;

@Controller
@RequestMapping("/factorial")
public class FactorialController {

    private final Logger log = LoggerFactory.getLogger(FactorialController.class);

    private final FactorialService factorialService;
    private final FileReader fileReader;

    @Autowired
    public FactorialController(FactorialService factorialService, FileReader fileReader) {
        this.factorialService = factorialService;
        this.fileReader = fileReader;
    }

    @GetMapping
    public String getFactorial(Model model) {

        AlgorithmData algorithmData = new AlgorithmData("", 1);

        model.addAttribute("result", 1.00);
        model.addAttribute("algorithmData", algorithmData);

        return "factorial";
    }

    @PostMapping
    public String getResult(@ModelAttribute AlgorithmData algorithmData, Model model) {

        double result = 0;
        String[] algorithm = new String[0];

        try {
            int value = Integer.parseInt(algorithmData.getValue());

            if (value >= 0) {
                String data;
                if (algorithmData.getType() == 1) {
                    result = factorialService.getResultIterative(value);
                    data = fileReader.readFromFile("factorialIterative");
                    algorithm = fileReader.prepareAlgorithmText(data, 1, 1);
                }
                if (algorithmData.getType() == 2) {
                    result = factorialService.getResultRecursive(value);
                    data = fileReader.readFromFile("factorialRecursive");
                    algorithm = fileReader.prepareAlgorithmText(data, 1, 1);
                }
            } else {
                model.addAttribute("exception", "value lower than 0");
            }
        } catch (IOException | NumberFormatException e) {
            log.error(e.getMessage());
            model.addAttribute("exception", e.getMessage());
        }

        model.addAttribute("algorithm", algorithm);
        model.addAttribute("algorithmData", algorithmData);
        model.addAttribute("result", result);

        return "factorial";
    }
}
