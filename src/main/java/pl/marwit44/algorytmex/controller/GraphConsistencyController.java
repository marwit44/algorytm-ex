package pl.marwit44.algorytmex.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import pl.marwit44.algorytmex.service.FileReader;
import pl.marwit44.algorytmex.service.GraphConsistencyService;

import java.io.IOException;
import java.util.Objects;

@Controller
@RequestMapping("/graphConsistency")
public class GraphConsistencyController {

    private final Logger log = LoggerFactory.getLogger(GraphConsistencyController.class);

    private final GraphConsistencyService graphConsistencyService;
    private final FileReader fileReader;

    @Autowired
    public GraphConsistencyController(GraphConsistencyService graphConsistencyService, FileReader fileReader) {
        this.graphConsistencyService = graphConsistencyService;
        this.fileReader = fileReader;
    }

    @GetMapping
    public String getGraphConsistency(Model model) {

        model.addAttribute("result", "");

        return "graphConsistency";
    }

    @PostMapping
    public String getResult(@ModelAttribute MultipartFile file, Model model) {

        String result = "";
        String[] valueAndColumns = new String[0];
        String[] firstPartAlgorithm = new String[0];
        String[] secondPartAlgorithm = new String[0];

        try {
            if (file != null) {
                String fileContent = "";
                if (Objects.equals(file.getContentType(), "text/plain")) {
                    fileContent = fileReader.readFromMultipartFile(file);
                } else {
                    model.addAttribute("exception", "uploaded file is not text file");
                }

                if (Objects.equals(file.getOriginalFilename(), "")) {
                    model.addAttribute("exception", "no file uploaded");
                }

                if (!fileContent.equals("")) {
                    String[] value = graphConsistencyService.prepareText(fileContent);
                    result = graphConsistencyService.checkConsistency(value);
                    valueAndColumns = graphConsistencyService.addColumnNumbers(fileContent);
                }

                model.addAttribute("fileName", file.getOriginalFilename());
            }
            String data = fileReader.readFromFile("graphConsistencyAlgorithm");
            firstPartAlgorithm = fileReader.prepareAlgorithmText(data, 1, 2);
            secondPartAlgorithm = fileReader.prepareAlgorithmText(data, 2, 2);
        } catch (IOException | NumberFormatException e) {
            log.error(e.getMessage());
            model.addAttribute("exception", e.getMessage());
        }

        model.addAttribute("firstPartAlgorithm", firstPartAlgorithm);
        model.addAttribute("secondPartAlgorithm", secondPartAlgorithm);
        model.addAttribute("secondPartIndex", firstPartAlgorithm.length);
        model.addAttribute("fileContent", valueAndColumns);
        model.addAttribute("result", result);

        return "graphConsistency";
    }
}
