package pl.marwit44.algorytmex.exceptions;

public class FileNotFoundException extends Throwable {
    public FileNotFoundException(String message) {
        super(message);
    }
}
