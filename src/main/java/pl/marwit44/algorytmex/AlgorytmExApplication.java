package pl.marwit44.algorytmex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlgorytmExApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgorytmExApplication.class, args);
	}
}
