package pl.marwit44.algorytmex.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PalindromeTest {

    private PalindromeService palindromeService;

    @Before
    public void init() {

        palindromeService = new PalindromeService();
    }

    @Test
    public void shouldCheckPalindromeWithCorrectResult() {

        //Given

        //When
        String value = palindromeService.prepareText("ala., ala");
        String result = palindromeService.checkText(value);

        //Then
        Assertions.assertThat(result.equals("Yes")).isTrue();
    }

    @Test
    public void shouldCheckPalindromeWithWrongResult() {

        //Given

        //When
        String value = palindromeService.prepareText("ala ma kota");
        String result = palindromeService.checkText(value);

        //Then
        Assertions.assertThat(result.equals("No")).isTrue();
    }
}
