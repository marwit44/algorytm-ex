package pl.marwit44.algorytmex.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GraphConsistencyTest {

    private GraphConsistencyService graphConsistencyService;

    @Before
    public void init() {

        graphConsistencyService = new GraphConsistencyService();
    }

    @Test
    public void shouldCheckGraphConsistencyWithCorrectResult() {

        //Given
        String[] str = {"0 1 0", "1 0 1", "0 1 0"};

        //When
        String result = graphConsistencyService.checkConsistency(str);

        //Then
        Assertions.assertThat(result.equals("Yes")).isTrue();
    }

    @Test
    public void shouldCheckGraphConsistencyWithWrongResult() {

        //Given
        String[] str = {"0 1 0", "1 0 0", "0 0 0"};

        //When
        String result = graphConsistencyService.checkConsistency(str);

        //Then
        Assertions.assertThat(result.equals("No")).isTrue();
    }
}
