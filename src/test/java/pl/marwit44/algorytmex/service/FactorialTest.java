package pl.marwit44.algorytmex.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FactorialTest {

    private FactorialService factorialService;

    @Before
    public void init() {

        factorialService = new FactorialService();
    }

    @Test
    public void shouldGetFactorialIterative() {

        //Given

        //When
        double result = factorialService.getResultIterative(4);

        //Then
        Assertions.assertThat(result == 24).isTrue();
    }

    @Test
    public void shouldGetFactorialRecursive() {

        //Given

        //When
        double result = factorialService.getResultIterative(6);

        //Then
        Assertions.assertThat(result == 720).isTrue();
    }
}