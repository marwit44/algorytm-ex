package pl.marwit44.algorytmex.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BubbleSortTest {

    private BubbleSortService bubbleSortService;

    @Before
    public void init() {

        bubbleSortService = new BubbleSortService();
    }

    @Test
    public void shouldGetSortedResult() {

        //Given
        String str = "10 -10 20 3";

        //When
        String result = bubbleSortService.getSortedResult(str);

        //Then
        Assertions.assertThat(result.equals("-10 3 10 20 ")).isTrue();
    }
}
